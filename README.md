## Traffic Light Miniature
Traffic Light Miniature adalah miniatur lampu lalu lintas sederhana yang diimplementasikan pada AVR. 
Nyala lampu terdiri atas lampu hijau dan lampu merah.

Fitur-Fitur:
- Time Counter: Untuk menampilkan durasi yang tersisa sebelum terjadi transisi lampu
- Mode: Fitur mode durasi nyala lampu
- Ubah durasi: Mengatur durasi lampu LED dari (1-9 seconds)

Cara Menggunakan:
- Untuk meng-switch mode lampu tekan tombol "MODE" 
- Untuk mengganti durasi nyala lampu, maka lakukan hal berikut:
	1. Tekan tombol "MENU/SAVE"
	2. Masukkan durasi yang diinginkan dengan menggunakan keypad
	3. Tekan tombol "MENU/SAVE"
	4. Tekkan tombol apapun di keypad untuk menyimpan durasi 

Kontributor:
- Mahardika Krisna Ihsani 
- Iqrar Agalosi Nureyza
	
