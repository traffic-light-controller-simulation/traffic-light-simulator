;
; Init keypad-I/O
.include "m8515def.inc"
;Initialize registers
.def col=r17
.def line=r18
.def tmp=r21 ;Temporary register
.def num=r20
.def display=r19
.def state=r22
.def timer_count=r24
.def led_mode=r25
.def selected_mode=r23 ;Register yang menunjuk mode yang sedang dipakai
.def screen_mode=r16 ;Register untuk menyimpan menu yang sedang digunakan
;==============
;Code Segment
;==============
;Main program
.org $00
  rjmp RESET
;External interrupt (tombol ganti menu & simpan data keypad ke SRAM)
.org $01
  rjmp ext_int0
;External interrupt (tombol untuk mengganti mode nyala LED)
.org $02
  rjmp ext_int1
.org $06
  rjmp ISR_TOV1

RESET:
  ;Inisiasi register menu dan mode
  ldi screen_mode,0
  ldi selected_mode,1
  ldi led_mode,0
  ;Inisiasi stack pointer
  ldi tmp, low(RAMEND)
  out SPL, tmp
  ldi tmp, high(RAMEND)
  out SPH, tmp

;Inisiasi interrupt
INIT_INTERRUPT:
  ;Inisiasi external interrupt
  ldi tmp,0b00001010
  out MCUCR,tmp
  ldi tmp,0b11000000
  out GICR,tmp
  sei
  ;[TODO!]	Buat inisiasi untuk timer interrupt

;Inisiasi SRAM, simpan nilai ascii 0 jika data pada SRAM belum terisi 
INIT_SRAM:
	 ldi XL,0x60
	 ldi XH,0x00
	 ld num,X
	 cpi num,0xFF
	 breq proc1
	 rjmp proc2
	 proc1:
		ldi state,0x31
		st X,state
	 proc2:
	 	subi XL,-1
		ld num,X
		cpi num,0xFF
		brne proc3
	    ldi state,0x31
		st X,state
	 proc3:
	 	subi XL,-1
		ld num,X
		cpi num,0xFF
		brne finished_proc
		ldi state,0x31
		st X,state
	 finished_proc:
	    subi XL,2
	
;Inisiasi LED di PORT E
INIT_LED:
	ser tmp 
	out DDRE,tmp 

;Inisiasi LCD
INIT_LCD_MAIN:
	;Panggil fungsi untuk inisiasi LCD
	rcall INIT_LCD
	ser tmp
	out DDRA,tmp ; Set port A as output
	out DDRB,tmp ; Set port B as output
	ldi ZH, high(2*title_00)
	ldi ZL, low(2*title_00)
	rcall WRITE_SENTENCE
	rcall NEWLINE
	ldi ZH, high(2*title_01)
	ldi ZL, low(2*title_01)
	rcall WRITE_SENTENCE
	rcall DELAY_02
	rcall DELAY_02
	rcall CLEAR_LCD

;Inisiasi keyoad
init_keypad:
	ldi tmp, 0b11110000 ; data direction register column lines output
	out DDRC, tmp    ; set direction register
	ldi tmp, 0b00001111 ; Pull-Up-Resistors to lower four port pins
	out PORTC, tmp    ; to output port
	;init untuk mengecek input keypads
	rcall write_text
	rcall write_num

;Main driver
main:
		;Cek nilai register yang menyimpan state menu, jika nilainya 0 berarti menu yang digunakan adalah menu untuk menyalakan LED
		cpi screen_mode,0
		;Jika bukan, alihkan ke menu untuk input keypad
		brne read_key
		timer_init:
			rcall CLEAR_LCD
			rcall write_text
			rcall write_num
			rcall write_elapsed
			rcall write_remaining
			;rcall write_remaining
			ldi col,0
			ldi tmp, (1<<CS11)	;
			out TCCR1B,tmp			
			ldi tmp,1<<TOV1
			out TIFR,tmp		; Interrupt if overflow occurs in T/C0
			ldi tmp,1<<TOIE1
			out TIMSK,tmp	
			sei	; Enable Timer/Counter0 Overflow int
		turn_led:
			cpi col,1
			breq main
			cpi led_mode,0
			breq mode_1
			cpi led_mode,1
			breq mode_2
			;Mode 1 lampu LED
			mode_1:
				;rcall write_remaining
				ldi num,0b0001001
				out PORTE,num
				rjmp turn_led
			;Mode 2 lampu LED
			mode_2:
				ldi num,0b0000110
				out PORTE,num
				rjmp turn_led

;Kodingan untuk membaca input keypad
read_key:
	ldi col, 4 
	ldi line, 1
	ldi tmp, 0b00001111 ; PB4..PB6=Null, pull-Up-resistors to input lines
	out PORTC, tmp    ; of port pins PB0..PB3
	in tmp, PINC    ; read key results
	ori tmp,0b11110000 ; mask all upper bits with a one
	cpi tmp,0b11111111 ; all bits = One?

	breq read_key        ; yes, no key is pressed

;Identifikasi karakter keypad yang ditekan 
ReadKey:
	ldi ZH,HIGH(2*KeyTable) ; Z is pointer to key code table
	ldi ZL,LOW(2*KeyTable)

	; read column 4
	ldi tmp, 0b01111111 ; PB7 = 0
	out PORTC, tmp
	in tmp, PINC ; read input line
	ori tmp, 0b11110000 ; mask upper bits
	cpi tmp, 0b11111111 ; a key in this column pressed?
	brne KeyRowFound ; key found
	adiw ZL,4 ; column not found, point Z one row down
	subi col, 1 ;decrement column

	; read column 3
	ldi tmp, 0b10111111 ; PB6 = 0
	out PORTC, tmp
	in tmp, PINC ; read input line
	ori tmp, 0b11110000 ; mask upper bits
	cpi tmp, 0b11111111 ; a key in this column pressed?
	brne KeyRowFound ; key found
	adiw ZL,4 ; column not found, point Z one row down
	subi col, 1 ;decrement column

	; read column 2
	ldi tmp, 0b11011111 ; PB5 = 0
	out PORTC, tmp
	in tmp, PINC ; read again input line
	ori tmp, 0b11110000 ; mask upper bits
	cpi tmp, 0b11111111 ; a key in this column?
	brne KeyRowFound ; column found
	adiw ZL,4 ; column not found, another four keys down
	subi col, 1 ;decrement column

	; read column 1
	ldi tmp, 0b11101111 ; PB4 = 0
	out PORTC, tmp
	in tmp, PINC ; read again input line
	ori tmp, 0b11110000 ; mask upper bits
	cpi tmp, 0b11111111 ; a key in this column?
	brne KeyRowFound ;column found
	ldi col, 4 ;balikin lagi column ke 4
	breq read_key ; unexpected: no key in this column pressed

KeyRowFound: ; column identified, now identify row
	lsr tmp ; shift a logic 0 in left, bit 0 to carry
	brcc KeyProc ; a zero rolled out, key is found
	adiw ZL,1 ; point to next key code of that column
	subi line, -1 ;increment line
	rjmp KeyRowFound ; repeat shift

;Memproses input keypad
KeyProc:
	lpm 
	;Simpan input ke register
	mov num,r0
	;LCD akan terus melakukan update & reset berdasarkan input angka yang dimasukkan user
	rcall CLEAR_LCD
	sbi PORTA, 1 
	out PORTB,num
	sbi PORTA,0
	cbi PORTA,0
	rcall DELAY_00
	ldi tmp, 0x20
	out PORTB,tmp
	sbi PORTA,0
	cbi PORTA,0
	rcall DELAY_00
	ldi tmp,0x73
	out PORTB,tmp
	sbi PORTA,0
	cbi PORTA,0
	rcall DELAY_00
	ldi tmp,0x65
	out PORTB,tmp
	sbi PORTA,0
	cbi PORTA,0
	rcall DELAY_00
	ldi tmp,0x63
	out PORTB,tmp
	sbi PORTA,0
	cbi PORTA,0
	rcall DELAY_00
	rjmp main

;Tampilkan angka untuk counter
write_elapsed:
	rcall NEWLINE
	ldi ZL,low(2*elapsed_text)
	ldi ZH,high(2*elapsed_text)
	rcall WRITE_SENTENCE
	ret
write_remaining:
	cbi PORTA,1 ; SETB RS
	ldi tmp, 0b11001001
	out PORTB,tmp
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	rcall DELAY_00
	ld tmp,X
	subi tmp,48
	sub tmp,timer_count
	subi tmp,-48
	sbi PORTA,1 
	out PORTB,tmp
	sbi PORTA,0
	cbi PORTA,0
	rcall DELAY_00
	ret
	
;Tulis string "MODE" ke LCD
write_text:
	rcall CLEAR_LCD
	ldi ZH,high(2*mode_text)
	ldi ZL,low(2*mode_text)
	rcall WRITE_SENTENCE
	ret

;Tulis string '<nama_mode>: <durasi>s' ke LCD
write_num:
		cpi selected_mode,1
		breq lcd_1
		cpi selected_mode,2
		breq lcd_2
		rjmp lcd_3
		lcd_1:
			ldi tmp,0x31
			out PORTB,tmp
			sbi PORTA,0
			cbi PORTA,0
			rcall DELAY_00
			rcall write_time
			ret
		lcd_2:
			ldi tmp,0x32
			out PORTB,tmp
			sbi PORTA,0
			cbi PORTA,0
			rcall DELAY_00
			rcall write_time
			ret
		lcd_3:
			ldi tmp,0x33
			out PORTB,tmp
			sbi PORTA,0
			cbi PORTA,0
			rcall DELAY_00
			rcall write_time
			ret

;Tampilkan durasi ke LCD
write_time:
	ldi tmp,0x3A
	out PORTB,tmp
	sbi PORTA,0
	cbi PORTA,0
	rcall DELAY_00
	ldi tmp,0x20
	out PORTB,tmp
	sbi PORTA,0
	cbi PORTA,0
	rcall DELAY_00
	;Ambil waktu yang tersisa dari SRAM
	ld tmp,X
	cpi tmp,0x39
	breq write_ten
	;TUliskan angka di antara 0-9
	subi tmp,-1
	out PORTB, tmp
	sbi PORTA,0
	cbi PORTA,0
	rcall DELAY_00
	rjmp write_text_finally
	;Tuliskan angka 10
	write_ten:
		ldi tmp,0x31
		out PORTB, tmp
		sbi PORTA,0
		cbi PORTA,0
		rcall DELAY_00
		ldi tmp,0x30
		out PORTB, tmp
		sbi PORTA,0
		cbi PORTA,0
		rcall DELAY_00
	write_text_finally:
		ldi tmp,0x20
		out PORTB,tmp
		sbi PORTA,0
		cbi PORTA,0
		rcall DELAY_00
		ldi ZL,low(2*sec_text)
		ldi ZH,high(2*sec_text)
		rcall WRITE_SENTENCE 
		ret

;External interrupt 1 (Untuk mengganti mode nyala LED)			 
ext_int1:
	cpi selected_mode,3
	brge reset_mode
	subi selected_mode,-1
	subi XL,-1
	;Reset timer
	ldi tmp,(0<<CS10)
	out TCCR1B,tmp
	ldi timer_count,0
	ldi col,1
	reti

;Reset nilai register mode apabila overflow(baca lebih dari 3)
reset_mode:
	ldi selected_mode,1
	subi XL,2
	ldi tmp,(0<<CS10)
	out TCCR1B,tmp
	ldi timer_count,0
	ldi col,1
	reti

;Timer interrupt
ISR_TOV1:
	ld tmp, X
	subi tmp,48
	;Jika sudah mencapai batas counter, maka reset counter ke 0
	cp timer_count,tmp
	breq reset_counter
	;Jika tidak, increment counter
	subi timer_count,-1
	rjmp isr_reset
	reset_counter:
		ldi timer_count,0
		rcall inc_led
	isr_reset:
		rcall write_remaining
		reti


inc_led:
	cpi led_mode,1
	breq one_state
	rjmp zero_state
	;Mode 1 lampu
	one_state:
		ldi led_mode,0
		ret
	;Mode 0 lampu
	zero_state:
		subi led_mode,-1
		ret

;Exteral interrupt 0
ext_int0:
	ldi timer_count,0
	tst screen_mode
	breq state_1
	rjmp state_0
	;Ganti menu ke menu untuk meminta input keypad 
	state_1:
		rcall CLEAR_LCD
		ldi tmp,(0<<CS11)
		out TCCR1B,tmp
		ldi ZL,low(2*durasi_text)
		ldi ZH,high(2*durasi_text)
		rcall WRITE_SENTENCE
		rcall NEWLINE
		ldi ZL,low(2*input_text)
		ldi ZH,high(2*input_text)
		rcall WRITE_SENTENCE
		ldi screen_mode,1
		ldi col,1
		reti

	;Ganti menu ke menu untuk menyalakan LED sekaligus simpan nilainya ke SRAM
 	state_0:
		ldi screen_mode,0
		st X,num
		;LCD menampilkan string untuk meminta pengguna menekan tombol apapun di keypad untuk menyimpan nilai durasi
		rcall CLEAR_LCD
		ldi ZL,low(2*key_text)
		ldi ZH,high(2*key_text)
		rcall WRITE_SENTENCE
		rcall NEWLINE
		ldi ZL,low(2*key2_text)
		ldi ZH,high(2*key2_text)
		rcall WRITE_SENTENCE
		reti

;Fungsi untuk menuliskan kaiimat ke LCD
WRITE_SENTENCE:    
  lpm              
  tst r0           
  breq write_quit  
  mov tmp, r0      
  rcall WRITE_CHAR
  adiw ZL,1        
  rjmp WRITE_SENTENCE
write_quit:
  ret
WRITE_CHAR:       
  sbi PORTA,1      
  out PORTB, tmp
  sbi PORTA,0	   
  cbi PORTA,0	   
  rcall DELAY_00
  ret

;Inisiasi LCD 
INIT_LCD:
	cbi PORTA,1 ; CLR RS
	ldi tmp,0x38 ; MOV DATA,0x38 --> 8bit, 2line, 5x7
	out PORTB,tmp
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	rcall DELAY_01 ;delay
	cbi PORTA,1 ; CLR RS
    ldi tmp,0b00001100 ; MOV DATA,0b00001100
	out PORTB,tmp ;delay
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	rcall DELAY_01 ;delay
	rcall CLEAR_LCD ; CLEAR LCD
	cbi PORTA,1 ; CLR RS
	ldi tmp,$06 ; MOV DATA,0x06 --> increase cursor, display sroll OFF
	out PORTB,tmp
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	rcall DELAY_01
	ret

NEWLINE:
	cbi PORTA,1 ; SETB RS
	ldi tmp, 0b11000000
	out PORTB,tmp
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	rcall DELAY_00
	ret

CLEAR_LCD:
	cbi PORTA,1 ; CLR RS
	ldi tmp,$01 ; MOV DATA,0x01
	out PORTB,tmp
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	rcall DELAY_00
	ret


	
DELAY_00:
	; Generated by delay loop calculator
	; at http://www.bretmulvey.com/avrdelay.html
	;
	; Delay 4 000 cycles
	; 500us at 8.0 MHz

	    ldi  r18, 6
	    ldi  r19, 49
	L0: dec  r19
	    brne L0
	    dec  r18
	    brne L0
	ret


DELAY_01:
	; Generated by delay loop calculator
	; at http://www.bretmulvey.com/avrdelay.html
	;
	; DELAY_CONTROL 40 000 cycles
	; 5ms at 8.0 MHz

	    ldi  r18, 52
	    ldi  r19, 242
	L1: dec  r19
	    brne L1
	    dec  r18
	    brne L1
	    nop
	ret

DELAY_02:
; Generated by delay loop calculator
; at http://www.bretmulvey.com/avrdelay.html
;
; Delay 160 000 cycles
; 20ms at 8.0 MHz

	    ldi  r18, 208
	    ldi  r19, 202
	L2: dec  r19
	    brne L2
	    dec  r18
	    brne L2
	    nop
		ret
;Title greeting
title_00:
.DB ">>TRAFFIC",0
title_01:
.DB "  SIMULATOR<<",0
;mode
mode_text:
.DB "MODE",0
sec_text:
.DB "sec",0
durasi_text:
.DB "Masukkan durasi",0
input_text:
.DB "(1-9)",0
key_text:
.DB "Tekan keypad",0
key2_text:
.DB "untuk lanjut",0

elapsed_text:
.DB "Counter: ",0
;Keypad mapping
KeyTable:
.DB 0x08,0x00,0x00,0x00 ; First column, keys *, 7, 4 und 1
.DB 0x33,0x36,0x39,0x00 ; second column, keys 0, 8, 5 und 2
.DB 0x32,0x35,0x38,0x30 ; third column, keys #, 9, 6 und 3
.DB 0x31,0x34,0x37,0x00 ; last column

